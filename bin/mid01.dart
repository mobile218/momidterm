import 'dart:io';
import 'dart:math';

String InputIns(String Insert) {
  return Insert = stdin.readLineSync()!;
}

List CutString(List Sent, String Insert) {
  return Sent = Insert.split(" ");
}

void ShowResult(List Sent) {
  return print(Sent);
}

void main(List<String> Arguments) {
  print('Input Your Insert : ');
  String Insert = " ";
  List Sent = [];
  Insert = InputIns(Insert);
  Sent = CutString(Sent, Insert);
  ShowResult(Sent);
  List Sent2 = Postfix(Sent);
  print(Sent2);
  print(Evaluate(Sent2));
}

List Postfix(List Sent) {
  List Operators = [];
  List Postfix = [];
  int Tokens = 0;
  int Operlast = 0;

  for (var i = 0; i < Sent.length; i++) {
    switch (Sent[i]) {
      case "0":
        Postfix.add(Sent[i]);
        break;
      case "1":
        Postfix.add(Sent[i]);
        break;
      case "2":
        Postfix.add(Sent[i]);
        break;
      case "3":
        Postfix.add(Sent[i]);
        break;
      case "4":
        Postfix.add(Sent[i]);
        break;
      case "5":
        Postfix.add(Sent[i]);
        break;
      case "6":
        Postfix.add(Sent[i]);
        break;
      case "7":
        Postfix.add(Sent[i]);
        break;
      case "8":
        Postfix.add(Sent[i]);
        break;
      case "9":
        Postfix.add(Sent[i]);
        break;
      default:
    }
    if (Sent[i] == "+" ||
        Sent[i] == "-" ||
        Sent[i] == "*" ||
        Sent[i] == "/" ||
        Sent[i] == "^") {
      switch (Sent[i]) {
        case "+":
          Tokens = 1;
          break;
        case "-":
          Tokens = 1;
          break;
        case "*":
          Tokens = 2;
          break;
        case "/":
          Tokens = 2;
          break;
        case "^":
          Tokens = 3;
          break;
        default:
      }
      if (Operators.isNotEmpty) {
        switch (Operators.last) {
          case "+":
            Operlast = 1;
            break;
          case "-":
            Operlast = 1;
            break;
          case "*":
            Operlast = 2;
            break;
          case "/":
            Operlast = 2;
            break;
          case "^":
            Operlast = 3;
            break;
          default:
        }
      }
      while (Operators.isNotEmpty && Operators.last != "(") {
        Postfix.add(Operators.removeLast());
      }
      Operators.add(Sent[i]);
    }
    if (Sent[i] == "(") {
      Operators.add(Sent[i]);
    }
    if (Sent[i] == ")") {
      while (Operators.last != "(") {
        Postfix.add(Operators.removeLast());
      }
      Operators.remove("(");
    }
  }
  while (Operators.isNotEmpty) {
    Postfix.add(Operators.removeLast());
  }
  return Postfix;
}

Evaluate(List Sent) {
  List<double> values = [];
  for (var i = 0; i < Sent.length; i++) {
    if (Sent[i] == "0" ||
        Sent[i] == "1" ||
        Sent[i] == "2" ||
        Sent[i] == "3" ||
        Sent[i] == "4" ||
        Sent[i] == "5" ||
        Sent[i] == "6" ||
        Sent[i] == "7" ||
        Sent[i] == "8" ||
        Sent[i] == "9") {
      double num = double.parse(Sent[i]);
      values.add(num);
    } else {
      double sum = 0;
      double right = values.last;
      values.removeLast();
      double left = values.last;
      values.removeLast();

      if (Sent[i] == "+") {
        sum = left + right;
      }
      if (Sent[i] == "-") {
        sum = left - right;
      }
      if (Sent[i] == "*") {
        sum = left * right;
      }
      if (Sent[i] == "/") {
        sum = left / right;
      }
      if (Sent[i] == "^") {
        sum = 0.0 + pow(left, right);
      }
      values.add(sum);
    }
  }
  return values[0];
}
